﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameCreatures.Models
{
    public class Commander
    {
        //Fields
        private readonly List<Creature> army;
        private string name;

        public Commander(string name, List<Creature> army)
        {
            this.Name = name;
            this.army = new List<Creature>();
            if (army == null)
            {
                throw new ArgumentNullException("Put some soldiers in, brother!");
            }
            this.army = army;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Name cannot be null.");
                }
                this.name = value;
            }
        }


        public int ArmySize
        {
            get
            {
                return this.army.Count;
            }
        }
        //Methods
        public void AttackAtPosition(Commander enemy, int attackerIndex, int targetIndex)
        {
            army[attackerIndex].Attack(enemy.army[targetIndex]);
            if (enemy.army[targetIndex].HealthPoints <= 0)
            {
                enemy.army.Remove(enemy.army[targetIndex]);
                targetIndex--;
            }
        }
        public void AutoAttack(Commander enemy)
        {

            int tempDmg = 0;
            int maxDmg = 0;
            int currentIndexEnemy = 0;
            int currentIndexAlly = 0;
            List<Creature> bestCrToDie = new List<Creature>();


            //for cycle with current creature
            //find the best creature to attack
            //fi

            for (int i = 0; i < army.Count; i++)
            {
                for (int j = 0; j < enemy.ArmySize; j++)
                {
                    var target = army[i].FindBestTarget(enemy.army);

                    tempDmg = army[i].CalculateActualDamage(target);
                    if (tempDmg >= maxDmg)
                    {
                        if (maxDmg != tempDmg)
                        {
                            bestCrToDie.Clear();
                        }
                        maxDmg = tempDmg;
                        currentIndexEnemy = j;
                        currentIndexAlly = i;
                        bestCrToDie.Add(target);
                    }
                }
            }
            if (bestCrToDie.Count > 1)
            {
                var cr = bestCrToDie.First(c => c.Damage == bestCrToDie.Max(i => i.Damage));
                currentIndexEnemy = enemy.army.IndexOf(cr);

            }


            AttackAtPosition(enemy, currentIndexAlly, currentIndexEnemy);

        }
    }
}
    

