﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameCreatures.Models
{
    public class Creature
    {
        //Fields
        private string name;
        private int damage;
        private AttackType attackType;
        private ArmorType armorType;
        private int healthPoints;

        //Constructor
        public Creature(string name, int damage, int healthPoints, AttackType attackType, ArmorType armorType)

        {
            this.Name = name;
            this.Damage = damage;
            this.AttackType = attackType;
            this.ArmorType = armorType;
            this.HealthPoints = healthPoints;

            if (this.HealthPoints <= 0)
            {
                throw new ArgumentOutOfRangeException("Health points must be a positive number");
            }
        }
        //Properties

        //DONE Name: string, should never be null.
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Name cannot be empty, please provide a valid name.");
                }

                this.name = value;
            }
        }

        //DONE Damage: int, should always be positive.
        public int Damage
        {
            get
            {
                return this.damage;
            }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("The damage should be a positive number.");
                }
                this.damage = value;
            }
        }

        //HealthPoints: int, initial(passed to constructor) HealthPoints should always be positive. 
        //HealthPoints have public setter, which keeps the HealthPoints at least at 0. 
        //(e.g. initial HealthPoints: 10, attack for 20 damage happens, HealthPoints are set to 0 instead of -10)
        public int HealthPoints
        {
            get
            {
                return this.healthPoints;
            }
            set
            {
                if (value <= 0)
                {
                    value = 0;
                }
                this.healthPoints = value;
            }
        }

        public ArmorType ArmorType
        {
            get
            {
                return this.armorType;
            }
            set
            {
                this.armorType = value;
            }
        }

        public AttackType AttackType
        {
            get
            {
                return this.attackType;
            }
            set
            {
                this.attackType = value;
            }
        }


        //Methods
        public void Attack(Creature target)
        {
            target.HealthPoints -= CalculateActualDamage(target);
        }

        public Creature FindBestTarget(List<Creature> targets)
        {
            {
                int currentHealth = int.MaxValue;
                int lowestHealth = int.MaxValue;
                List<Creature> killedCreatures = new List<Creature>();
                List<Creature> creaturesLowerHelth = new List<Creature>();

                var cretureLowestHelt = new Creature("defauls", 1, 1, default, default);

                foreach (var item in targets)
                {

                    currentHealth = item.HealthPoints - CalculateActualDamage(item);
                    if (currentHealth <= 0)
                    {
                        currentHealth = 0;
                        killedCreatures.Add(item);

                    }
                    if (currentHealth <= lowestHealth)
                    {

                        if (lowestHealth != currentHealth)
                        {
                            creaturesLowerHelth.Clear();
                        }
                        creaturesLowerHelth.Add(item);
                        lowestHealth = currentHealth;
                    }
                }
                if (killedCreatures.Count != 0)
                {
                    return killedCreatures.First(c => c.Damage == killedCreatures.Max(i => i.Damage));
                }
                else
                {
                    return creaturesLowerHelth.First(c => c.Damage == creaturesLowerHelth.Max(i => i.Damage));
                }
            }
        }
        public void AutoAttack(List<Creature> targets)
        {
            Attack(FindBestTarget(targets));
        }

        public int CalculateActualDamage(Creature target)
        {
            if (target.ArmorType == ArmorType.Light)
            {
                if (this.AttackType == AttackType.Ranged)
                {
                    return (int)Math.Floor(this.damage * 1.25);
                }
                else if (this.AttackType == AttackType.Melee)
                {
                    return (int)Math.Floor(this.damage * 1.00);
                }
                else if (this.AttackType == AttackType.Magic)
                {
                    return (int)Math.Floor(this.damage * 0.75);
                }
            }
            else if (target.ArmorType == ArmorType.Medium)
            {
                if (this.AttackType == AttackType.Ranged)
                {
                    return (int)Math.Floor(this.damage * 1.00);
                }
                else if (this.AttackType == AttackType.Melee)
                {
                    return (int)Math.Floor(this.damage * 1.25);
                }
                else if (this.AttackType == AttackType.Magic)
                {
                    return (int)Math.Floor(this.damage * 1.00);
                }
            }
            else if (target.ArmorType == ArmorType.Heavy)
            {
                if (this.AttackType == AttackType.Ranged)
                {
                    return (int)Math.Floor(damage * 0.75);
                }
                else if (this.AttackType == AttackType.Melee)
                {
                    return (int)Math.Floor(this.damage * 0.75);
                }
                else if (this.AttackType == AttackType.Magic)
                {
                    return (int)Math.Floor(this.damage * 1.25);
                }
            }
            return this.damage;
        }
    }

}
