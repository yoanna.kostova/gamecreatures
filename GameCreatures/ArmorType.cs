﻿namespace GameCreatures
{
    public enum ArmorType
    {
        Light = 0,
        Medium = 1,
        Heavy = 2
    }
}
